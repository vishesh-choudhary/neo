# Rust Graphql Service Boilerplate

This Provides A Simple GraphQL Boilerplate For Services. It Uses:
- [actix-web](https://actix.rs/)
- [juniper](https://github.com/graphql-rust/juniper)
- [mongodb-cursor-pagination](https://github.com/briandeboer/mongodb-cursor-pagination)


Just Change/Add To The Models, Services And The Graphql_Schema Accorindgly.

### Usage

Seed Some Data With...
```
cargo run --bin seed
```

Run The Server With...
```
cargo run
```

And Then Test At:
```
http://localhost:8080/graphiql
```

#### Sample Query For Pets
```
{
  allPets(limit:4){
    pageInfo{
      startCursor
      nextCursor
      hasPreviousPage
      hasNextPage
    }
    pets{
      name
      id
      age
      petType
      gender
      owner{
        id
        username
      }
    }
    totalCount
  }
}
```

#### Sample Query For Owners
```
{
  allOwners {
    pageInfo {
      startCursor
      nextCursor
    }
    owners {
      id
      firstName
      lastName
      pets {
        id
        name
      }
    }
  }
}
```

## Inspiration And Some Resources To Help
- [Example using juniper and diesel(SQL)](https://dev.to/open-graphql/building-powerful-graphql-servers-with-rust-3gla)
- [Mongodb cursor pagination](https://github.com/briandeboer/mongodb-cursor-pagination)
- [Details about paging](https://engineering.mixmax.com/blog/api-paging-built-the-right-way/)
- [More information about mongodb](http://alex.amiran.it/post/2018-08-16-rust-graphql-webserver-with-warp-juniper-and-mongodb.html)
- [Another mongodb juniper example](https://github.com/shareeff/rust_graphql_mongodb)